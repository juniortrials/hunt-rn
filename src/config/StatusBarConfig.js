import { StatusBar } from "react-native";

// somente Android
StatusBar.setBackgroundColor("#DA552F");

// somente IOS
StatusBar.setBarStyle("light-content");
